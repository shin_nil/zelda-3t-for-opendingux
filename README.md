The Legend of Zelda: Time to Triumph (OpenDingux Version [A320/GCW-Zero])
================================================================
![Title Screen](http://www.zeldaroth.fr/images/3t/s1.png)

This is the third game of a trilogy developed by fans. My intention to build all the three for OpenDingux is now complete :)

Original code: [Vincent Jouillat](http://www.zeldaroth.fr)
--------------

History / Changelog
-------------------
###(02/09/2013)
* GCW-Zero alpha release


Controls
--------

###Menus

* Move the pointer - D-Pad;
* Confirm - Start;
* Quit the game - Select.

###Game Play

* Move Link - D-Pad
* Run - Hold A (if you own boots)
* Use the sword - B (if you own a sword)
* Great technique - B to load, release when fully charged (after the tone) to use a spin attack.
* Look around - R + D-Pad (outside dungeons)
* Use selected object - Y (only when the selected object is useable)
* Use gloves - A
* See the map - X (outside or in a dungeon)
* See defeated monsters - L (after finding it)
* See fish list - L
* See merchandise list - L
* Oni Link transform -L
* Open a chest/Read/Speak - B
* Select/Confirm/Pass - Start
* Access/Quit the selection item menu - Start
* Quit/Save - Select


For more information about the game, including Walkthroughs, visit the creators: [http://www.zeldaroth.fr/us/index.php](http://www.zeldaroth.fr/us/index.php)  
For more information about the OpenDingux port, visit my page (portuguese): [http://www.shinnil.blogspot.com.br](http://www.shinnil.blogspot.com.br)  
For more information about OpenDingux (A320), visit [http://www.treewalker.org/opendingux/](http://www.treewalker.org/opendingux/)  
For more information about GCW-Zero, visit [http://www.gcw-zero.com/](http://www.gcw-zero.com/) 

